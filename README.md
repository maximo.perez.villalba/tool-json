# tool-json

PHP Persistencia en archivos de texto en formato Json. 

Para acceder a leer y escribir el contenido de un archivo en formato Json usar:
[**codemax\tool\json\files\Json**](https://gitlab.com/code.max/tool-json/-/blob/master/src/codemax/tool/json/files/Json.php) 

Para acceder a la estructura interna del archivo y/o modificarlo usar
[**codemax\tool\json\entities\JsonEntity**](https://gitlab.com/code.max/tool-json/-/blob/master/src/codemax/tool/json/entities/JsonEntity.php)

JsonEntity permite mapear secciones internas del archivo en memoria a través de 
[**codemax\tool\json\entities\models\JsonModel**](https://gitlab.com/code.max/tool-json/-/blob/master/src/codemax/tool/json/entities/models/JsonModel.php)


